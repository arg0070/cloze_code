<?php
$string['pluginname'] = 'CodeHL';
$string['codehl_desc'] = 'Give format to selected code';
$string['codehle_desc'] = 'Export your CLOZE questions';
$string['title'] = 'CodeHL';
$string['error'] = 'You have not selected any code';
$string['errordesc1'] = 'First select the code to format.';
$string['errordesc2'] = 'Then press the icon ';
$string['inlineCSS'] = 'Check this box to include CSS styles inline. ';
$string['inlineCSSHelp'] = 'Select this option if you do not have the CSS file in your web.';
$string['indent'] = 'Disable Auto-indent';
$string['indentHelp'] = 'Select this option if you do not want to indent the code or the indentation is not correct.';
$string['preview'] = 'Preview';
$string['options'] = 'Options';
$string['website'] = 'Project website';
$string['websitehelp'] = 'Find more help';
$string['help'] = 'Help?';
