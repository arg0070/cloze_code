<?php

$string['pluginname'] = 'CodeHL';
$string['codehl_desc'] = 'Formatea el codigo seleccionado';
$string['codehle_desc'] = 'Exporta tus preguntas CLOZE';
$string['title'] = 'CodeHL';
$string['error'] = 'No has seleccionado ningun c�digo.';
$string['errordesc1'] = 'Primero selecciona el c�digo a formatear.';
$string['errordesc2'] = 'Despu�s pulsa el icono ';
$string['inlineCSS'] = 'Selecciona esta opci�n para mostrar los estilos CSS en l�nea. ';
$string['inlineCSSHelp'] = 'Selecciona esta opci�n si no tienes el archivo CSS en tu Web.';
$string['indent'] = 'Disable Auto-indent';
$string['indentHelp'] = 'Selecciona esta opci�n si no quieres formatear el c�digo o la indentaci�n no es correcta.';
$string['preview'] = 'Vista previa';
$string['options'] = 'Opciones';
$string['website'] = 'Web del proyecto';
$string['websitehelp'] = 'Encuentra m�s ayuda';
$string['help'] = '�Ayuda?';
