/**
 * 
 * editor_plugin_src.js
 */
(function() {
    // Load plugin specific language pack
    tinymce.create('tinymce.plugins.CodeHighLighterPlugin', {
        /**
         * Initializes the plugin, this will be executed after the plugin has been created.
         * This call is done before the editor instance has finished it's initialization so use the onInit event
         * of the editor instance to intercept that event.
         *
         * @param {tinymce.Editor} ed Editor instance that the plugin is initialized in.
         * @param {string} url Absolute URL to where the plugin is located.
         */
        init: function(ed, url) {

            /**
             * Command to export CLOZE questions.
             */
            ed.addCommand('clozeExport', function() {
                var content = ed.getContent({format: 'raw'});
                // Question format.
                question = '{[0-9.]*:[A-Z.\_?]+:[^}]*}';
                find = new RegExp(question, 'g');
                list = content.match(find);
                // Array to save question's values and feedback.
                I = 'I = new Array;';
                if (list === null) {
                    alert("CLOZE question not found");
                    return;
                }
                for (var i = 0; i < list.length; i++) {
                    elements = extractElements(list[i]);
                    content = content.replace(list[i], htmlOutput(i, elements));
                    encodeAnswers(i, elements.answers);
                }

                // Create a new window to try CLOZE questions.
                var opened = window.open("");
                opened.document.write(getHeader() + getBody(content));

                /**
                 * Extract the question's elements
                 * @param {string} cloze_question Question in CLOZE format
                 * @returns questions's elements
                 */
                function extractElements(cloze_question) {
                    subexp = new RegExp('[a-zA-Z0-9.\_?]+');

                    expWeight = new RegExp('{[0-9.]*:');
                    expType = new RegExp(':[A-Z.\_?]+:');
                    expAnswers = new RegExp('%[^}]+');

                    weight = cloze_question.match(expWeight);
                    type = cloze_question.match(expType);
                    list_answers = cloze_question.match(expAnswers);

                    var elements = {
                        weight: weight.toString().match(subexp),
                        type: type.toString().match(subexp),
                        answers: extractAnswers(list_answers.toString())};
                    return elements;
                }

                /**
                 * Extract the different asnwers for a question
                 * @param {type} answers answer
                 * @returns {Array} value, asnwer and feedback for every asnwer
                 */
                function extractAnswers(answers) {
                    var list = new Array;

                    textValue = '%[0-9.]+%';
                    textAnswer = '%[a-zA-Z.]+';
                    textFeedback = '#[a-zA-Z0-9.]*';

                    subexp = new RegExp('[a-zA-Z0-9.]+');
                    expValue = new RegExp(textValue, 'g');
                    expAnswer = new RegExp(textAnswer, 'g');
                    expFeedback = new RegExp(textFeedback, 'g');

                    list_values = answers.match(expValue);
                    list_answers = answers.match(expAnswer);
                    list_feedback = answers.match(expFeedback);
                    for (var i = 0; i < list_values.length; i++) {
                        list[i] = new Array;
                        list[i][0] = list_values[i].match(subexp);
                        list[i][1] = list_answers[i].match(subexp);
                        list[i][2] = (list_feedback[i].match(subexp) === null ? '' : list_feedback[i].match(subexp));
                    }
                    return list;
                }

                /**
                 * Generate the HTML expressions for a question.
                 * @param {type} id question id
                 * @param {type} question question values
                 * @returns {String} HTML code
                 */
                function htmlOutput(id, question) {
                    switch (question.type.toString()) {
                        case 'SHORTANSWER':
                        case 'SHORTANSWER_C':
                            return '<input type="text" id="quest_' + id + '"><span id="span_' + id + '"></span>';
                        case 'MULTICHOICE':
                            var options = '';
                            for (var i = 0; i < question.answers.length; i++) {
                                options += '<option>' + question.answers[i][1] + '</option>';
                            }
                            return '<select id="quest_' + id + '">' + options + '</select><span id="span_' + id + '"></span>';
                        case 'MULTICHOICE_H':
                        case 'MULTICHOICE_V':
                            var radio = '';
                            for (var i = 0; i < question.answers.length; i++) {
                                radio += '<input type="radio" name="radio" id="quest_' + id + '" value="' + question.answers[i][1] + '">' + question.answers[i][1];
                            }
                            return radio + '<span id="span_' + id + '"></span>';
                    }
                }

                /**
                 * Encode the question's answers and feedback.
                 * @param {type} str text to encode
                 * @returns {String} unicode equivalence
                 */
                function toUnicode(str) {
                    var unicodeString = '';
                    for (var i = 0; i < str.length; i++) {
                        var unicode = str.charCodeAt(i).toString(16).toUpperCase();
                        while (unicode.length < 4) {
                            unicode = '0' + unicode;
                        }
                        unicode = '\\u' + unicode;
                        unicodeString += unicode;
                    }
                    return unicodeString;
                }

                /**
                 * Encode and save asnwers and feedback
                 * @param {type} id question id
                 * @param {type} questions question
                 * @returns void
                 */
                function encodeAnswers(id, questions) {
                    I += 'I[' + id + '] = new Array;';
                    for (var i = 0; i < questions.length; i++) {
                        I += 'I[' + id + '][' + i + '] = new Array; I[' + id + '][' + i + '][0] = ' + questions[i][0] + '; I[' + id + '][' + i + '][1] = "' + toUnicode(questions[i][1].toString()) + '"; I[' + id + '][' + i + '][2] = "' + toUnicode(questions[i][2].toString()) + '";';
                    }
                }

                /**
                 * Print the header of the export window.
                 * @returns {String} HTML header
                 */
                function getHeader() {
                    var header = {
                        meta: '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">',
                        title: '<title>Cloze Test</title>',
                        css: '<style>.has-error{border-color:#A94442}.has-success{border-color:#3C763D}pre{display:block;padding:9.5px;margin:0 0 10px;font-size:13px;line-height:1.42857143;word-break:break-all;word-wrap:break-word;color:#333;background-color:#f5f5f5;border:1px solid #ccc;border-radius:4px}.container{max-width: 970px;margin-right:auto;margin-left:auto;padding-left:5px;padding-right:5px;}.pln{color:#000}@media screen{.str{color:#080}.kwd{color:#008}.com{color:#800}.typ{color:#606}.lit{color:#066}.clo,.opn,.pun{color:#660}.tag{color:#008}.atn{color:#606}.atv{color:#080}.dec,.var{color:#606}.fun{color:red}}@media print,projection{.str{color:#060}.kwd{color:#006;font-weight:700}.com{color:#600;font-style:italic}.typ{color:#404;font-weight:700}.lit{color:#044}.clo,.opn,.pun{color:#440}.tag{color:#006;font-weight:700}.atn{color:#404}.atv{color:#060}}pre.prettyprint{padding:2px;border:1px solid #888}ol.linenums{margin-top:0;margin-bottom:0}li.L0,li.L1,li.L2,li.L3,li.L5,li.L6,li.L7,li.L8{list-style-type:none}li.L1,li.L3,li.L5,li.L7,li.L9{background:#eee}</style>',
                        script: '<script>' + I + 'function checkAnswers(){var e=0;var t=0;while((a=document.getElementById("quest_"+e+""))!==null){for(var n=0;n<I[e].length;n++){if(I[e][n][0]===100&&a.value===I[e][n][1]){isCorrect(e,I[e][n][2]);t+=I[e][n][0];break}else if(a.value===I[e][n][1]){isWrong(e,I[e][n][2]);break}if(n===I[e].length-1)isWrong(e,"Incorrecto")}e++}alert("Tu resultado final: "+t+" puntos.")}function isCorrect(e,t){var n=document.getElementById("quest_"+e+"").className="has-success";var r=document.getElementById("span_"+e+"").innerHTML=t}function isWrong(e,t){document.getElementById("quest_"+e+"").className="has-error";var n=document.getElementById("span_"+e+"").innerHTML=t}</script>'
                    };
                    return '<!DOCTYPE html><html><head>' + header.meta + header.title + header.css + header.script + '</head>';
                }

                /**
                 * Print the body of the export window
                 * @param {type} content body content
                 * @returns HTML body
                 */
                function getBody(content) {
                    return '<body><div class="container">' + content + '</div><div class="container"><button id="check" onclick="checkAnswers();">Check!</button></div></body></html>';
                }
            });

            /**
             * Command to show the Code preview window
             */
            ed.addCommand('mcecodehl', function() {
                ed.windowManager.open({
                   	file : ed.getParam("moodle_plugin_base") + 'codehl/dialog.php',
                    width: 780,
                    height: 400,
                    inline: 1,
                    scrollbars: true}, {plugin_url: url})
            });

            // Register buttons
            ed.addButton('codehl', {
                title: 'codehl.codehl_desc',
                cmd: 'mcecodehl',
                image: url + '/img/codehl.gif'});

            ed.addButton('clozeexport', {
                title: 'codehl.codehle_desc',
                cmd: 'clozeExport',
                image: url + '/img/codehle.gif'});

            ed.onNodeChange.add(function(ed, cm, n) {
                cm.setActive('codehl', n.name);
            });
        },
        /**
         * Creates control instances based in the incomming name. This method is normally not
         * needed since the addButton method of the tinymce.Editor class is a more easy way of adding buttons
         * but you sometimes need to create more complex controls like listboxes, split buttons etc then this
         * method can be used to create those.
         *
         * @param {String} n Name of the control to create.
         * @param {tinymce.ControlManager} cm Control manager to use inorder to create new control.
         * @return {tinymce.ui.Control} New control instance or null if no control was created.
         */
        createControl: function(n, cm) {
            return null;
        },
        /**
         * Returns information about the plugin as a name/value array.
         * The current keys are longname, author, authorurl, infourl and version.
         *
         * @return {Object} Name/value array containing information about the plugin.
         */
        getInfo: function() {
            return{longname: 'Code HighLigter', author: 'arg0070', authorurl: 'arg0070.bitbucket.org', infourl: 'arg0070.bitbucket.org', version: "1.0"}
        }});
    // Register the plugin
    tinymce.PluginManager.add('codehl', tinymce.plugins.CodeHighLighterPlugin);
})();

