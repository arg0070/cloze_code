var CodeHightLighter = {
    init: function() {
        // Text selected
        var code = tinyMCEPopup.editor.selection.getContent({format: 'text'});
        // Check if the text is empty
        if (code === "") {
            document.getElementById("errorPanel").className = "visible";
        } else {
            document.getElementById("textPanel").className = "visible";
            preFormat(code);
            document.getElementById('indent').addEventListener('click', function() {
                preFormat(code);
            }, false);
        }
        /**
         * Prepare the code to be formated.
         * @param {string} str code
         * @return {void}
         */
        function preFormat(str) {
            // Code indent
            if (!document.getElementById("indent").checked) {
                str = jspretty({source: str, insize: 4, preserve: true, space: false});
            }
            str = str.replace(/</g, "&lt;");
            str = str.replace(/>/g, "&gt;");
            str = str.replace(/\n/g, "<br/>");
            str = str.replace(/\s/g, '&nbsp;');
            document.getElementById("codearea").innerHTML = '<pre class="prettyprint"> ' + str + '</pre>';
            prettyPrint();
        }
    },
    insert: function() {
        // Get the final state of the code.
        var code = document.getElementById('codearea').innerHTML;
        // Include CSS styles inline.
        if (document.getElementById("inlineCSS").checked) {
            code = posFormat(code);
        }
        tinyMCEPopup.editor.execCommand('mceInsertContent', false, code);
        tinyMCEPopup.close();

        /**
         * Include CSS styles inline
         * @param {string} str code
         */
        function posFormat(str) {
            styles = parseCSS("prettify.css");
            if (styles != null) {
                for (var x = 0; x < styles.length; x++) {
                    text = 'class="' + styles[x].rule + '"';
                    find = new RegExp(text, 'g');
                    str = str.replace(find, 'style="' + styles[x].text + '"');
                }
            }
            return str;
        }
    }
};
tinyMCEPopup.onInit.add(CodeHightLighter.init, CodeHightLighter);
