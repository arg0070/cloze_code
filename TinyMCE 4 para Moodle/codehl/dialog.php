<?php // $Id: insert_cloze.php,v 1.4 2013/18/03

define('NO_MOODLE_COOKIES', true); // Session not used here.

require(dirname(dirname(dirname(dirname(dirname(dirname(__FILE__)))))) . '/config.php');

$PAGE->set_context(context_system::instance());
$PAGE->set_url('/lib/editor/tinymce/plugins/codehl/dialog.php');
$stringmanager = get_string_manager();

$editor = get_texteditor('tinymce');
$plugin = $editor->get_plugin('codehl');

$htmllang = get_html_lang();
header('Content-Type: text/html; charset=utf-8');
header('X-UA-Compatible: IE=edge');
?>
<!DOCTYPE html>
<html <?php echo $htmllang ?>
<head>
    <title><?php print_string('title', 'tinymce_codehl'); ?></title>
	<title>Code Editor</title>
<script type="text/javascript" src="<?php echo $editor->get_tinymce_base_url(); ?>tiny_mce_popup.js"></script>
<script type="text/javascript" src="<?php echo $plugin->get_tinymce_file_url('js/dialog.js'); ?>"></script>
<script type="text/javascript" src="<?php echo $plugin->get_tinymce_file_url('js/parse.js'); ?>"></script>
<script type="text/javascript" src="<?php echo $plugin->get_tinymce_file_url('js/jspretty.js'); ?>"></script>
<script type="text/javascript" src="<?php echo $plugin->get_tinymce_file_url('js/prettify/prettify.js'); ?>"></script>

<meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="<?php echo $plugin->get_tinymce_file_url('js/prettify/prettify.css'); ?>">
</head>
    <body onload ="CodeHightLighter.init();">
	<section class="container">
	    <nav class="navbar">
		<div class="navbar-header col-sm-1">
		    <a href="http://www.ubu.es" title="UBU">
			<img src="<?php echo $plugin->get_tinymce_file_url('img/ubu.png'); ?>" alt="UBU" title="UBU"/>
		    </a>
		</div>
		<ul class="nav navbar-nav">
		    <li><a href="http://arg0070.bitbucket.org" title="Project Website"><?php print_string('website', 'tinymce_codehl'); ?></a></li>
		    <li><a href="http://arg0070.bitbucket.org#help" title="Find more help"><?php print_string('websitehelp', 'tinymce_codehl'); ?></a></li>
		</ul>
	    </nav>
	    <br/>
	    <div class="row">
		<div id="errorPanel" class="hidden">
		    <div class="title"><?php print_string('error', 'tinymce_codehl'); ?></div>
		    <ol>
			<li><?php print_string('errordesc1', 'tinymce_codehl'); ?></li>
			<li><?php print_string('errordesc2', 'tinymce_codehl'); ?><img src="<?php echo $plugin->get_tinymce_file_url('img/codehl.gif'); ?>" title="icon" alt="icon"/></li>
		    </ol>           
		</div>
		<div id="textPanel" class="hidden">
		    <div class="title"><?php print_string('title', 'tinymce_codehl'); ?></div>
		    <br/>
		    <fieldset>
			<legend><?php print_string('preview', 'tinymce_codehl'); ?></legend>
			<div id="codearea" class="col-sm-3"></div>
		    </fieldset>
		    <br/>
		    <div id="optionsPanel" class="grid">
			<fieldset>
			    <legend><?php print_string('options', 'tinymce_codehl'); ?></legend>
			    <label for="indent">
				<input type="checkbox" id="indent" value="0"/>
			      <?php print_string('indent', 'tinymce_codehl'); ?><abbr title="<?php print_string('indentHelp', 'tinymce_codehl'); ?>">
				    <strong><?php print_string('help', 'tinymce_codehl'); ?></strong>
				</abbr>
			    </label>
			    <br/>
			    <label for="inlineCSS">
				<input type="checkbox" id="inlineCSS" value="0"/>
			       <?php print_string('inlineCSS', 'tinymce_codehl'); ?><abbr title="<?php print_string('inlineCSSHelp', 'tinymce_codehl'); ?>">
				    <strong><?php print_string('help', 'tinymce_codehl'); ?></strong>
				</abbr>
			    </label>                            
			</fieldset>
		    </div>
		    <br/>
 <div id="buttonsPanel" class="grid">
                        <input type="button" id="insert" value="Insert"
                               onclick="CodeHightLighter.insert();"/>
                        <input type="button" id="cancel" value="Cancel"
                               onclick="tinyMCEPopup.close();"/>
                    </div>       
		</div>
	    </div>
	</section>
    </body>
</html>
