<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Code Editor</title>
        <script src="../../tiny_mce_popup.js"></script>
        <script src="js/dialog.js"></script>
        <script src="js/parse.js"></script>
        <script src="js/jspretty.js"></script>
        <script src="js/prettify/prettify.js"></script>
        <link rel="stylesheet"  title="prettify" href="js/prettify/prettify.css">
        <link rel="stylesheet"  href="css/bootstrap.css">
    </head>
    <body>
        <section class="container">
            <nav class="navbar">
                <div class="navbar-header col-sm-1">
                    <a href="http://www.ubu.es" title="UBU">
                        <img src="img/ubu.png" alt="UBU" title="UBU"/>
                    </a>
                </div>
                <ul class="nav navbar-nav">
                    <li><a href="http://arg0070.bitbucket.org" title="{#codehl.website}">{#codehl.website}</a></li>
                    <li><a href="http://arg0070.bitbucket.org#help" title="{#codehl.websitehelp}">{#codehl.websitehelp}</a></li>
                </ul>
            </nav>
            <br/>
            <div class="row">
                <div id="errorPanel" class="hidden">
                    <div class="title">{#codehl.error}</div>
                    <ol>
                        <li>{#codehl.errordesc1}</li>
                        <li>{#codehl.errordesc2} <img src="img/codehl.gif" title="icon" alt="icon"/></li>
                    </ol>
                    <div class="col-sm-1">
                        <input type="button" id="cancel" value="{#codehl.back}"
                               onclick="tinyMCEPopup.close();"/>
                    </div>
                </div>
                <div id="textPanel" class="hidden">
                    <div class="title">{#codehl.title}</div>
                    <br/>
                    <fieldset>
                        <legend>{#codehl.preview}</legend>
                        <div id="codearea" class="col-sm-3"></div>
                    </fieldset>
                    <br/>
                    <div id="optionsPanel" class="grid">
                        <fieldset>
                            <legend>{#codehl.options}</legend>
                            <label for="inlineCSS">
                                <input type="checkbox" id="inlineCSS" value="0"/>
                                {#codehl.inlineCSS}<abbr title="{#codehl.inlineCSSHelp}">
                                    <strong>{#codehl.help}</strong>
                                </abbr>
                            </label>
                            <br/>
                            <label for="indent">
                                <input type="checkbox" id="indent" value="0"/>
                                {#codehl.indent}<abbr title="{#codehl.indentHelp}">
                                    <strong>{#codehl.help}</strong>
                                </abbr>
                            </label>
                        </fieldset>
                    </div>
                    <br/>
                    <div id="buttonsPanel" class="grid">
                        <input type="button" id="insert" value="{#codehl.insert}"
                               onclick="CodeHightLighter.insert();"/>
                        <input type="button" id="cancel" value="{#codehl.cancel}"
                               onclick="tinyMCEPopup.close();"/>
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>