<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

$string['codehl:codehl_desc'] = 'Formatea el código seleccionado';
$string['codehl:codehle_desc'] = 'Exporta tus preguntas CLOZE';
$string['codehl:back'] = 'Volver';
$string['codehl:cancel'] = 'Cancelar';
$string['codehl:insert'] = 'Insertar';
$string['codehl:title'] = 'Vista previa';
$string['codehl:error'] = 'No has seleccionado ningún código.';
$string['codehl:errordesc1'] = 'Primero selecciona el código a formatear.';
$string['codehl:errordesc2'] = 'Después pulsa el icono ';
$string['codehl:inlineCSS'] = 'Selecciona esta opción para mostrar los estilos CSS en línea. ';
$string['codehl:inlineCSSHelp'] = 'Selecciona esta opción si no tienes el archivo CSS en tu Web.';
$string['codehl:indent'] = 'Desactivar indentación automática. ';
$string['codehl:indentHelp'] = 'Selecciona esta opción si no quieres formatear el código o la indentación no es correcta.';
$string['codehl:preview'] = 'Vista previa';
$string['codehl:options'] = 'Opciones';
$string['codehl:website'] = 'Web del proyecto';
$string['codehl:websitehelp'] = 'Encuentra más ayuda';
$string['codehl:help'] = '¿Ayuda?';

