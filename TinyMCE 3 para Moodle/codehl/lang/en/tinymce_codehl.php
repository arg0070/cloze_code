<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

$string['codehl:questnotfoud'] = 'CLOZE question not found';
$string['codehl:codehl_desc'] = 'Give format to selected code';
$string['codehl:codehle_desc'] = 'Export your CLOZE questions';
$string['codehl:back'] = 'Go back';
$string['codehl:cancel'] = 'Cancel';
$string['codehl:insert'] = 'Insert';
$string['codehl:title'] = 'Code Preview';
$string['codehl:error'] = 'You have not selected any code.';
$string['codehl:errordesc1'] = 'First select the code to format.';
$string['codehl:errordesc2'] = 'Then press the icon.';
$string['codehl:inlineCSS'] = 'Check this box to include CSS styles inline. ';
$string['codehl:inlineCSSHelp'] = 'Select this option if you do not have the CSS file in your web.';
$string['codehl:indent'] = 'Disable Auto-indent. ';
$string['codehl:indentHelp'] = 'Select this option if you do not want to indent the code or the indentation is not correct.';
$string['codehl:preview'] = 'Preview';
$string['codehl:options'] = 'Options';
$string['codehl:website'] = 'Project website';
$string['codehl:websitehelp'] = 'Find more help';
$string['codehl:help'] = 'Help?';
