<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

defined('MOODLE_INTERNAL') || die();


$plugin->version = 2014061800;
$plugin->requires = 2011120500; 
$plugin->component = 'tinymce_codehl';
$plugin->maturity = MATURITY_STABLE;
$plugin->release = '1.0';
